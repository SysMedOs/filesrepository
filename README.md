[Link Text](Link URL)# LPPtiger related files #

This repository contains the related files of [LPPtiger](https://bitbucket.org/SysMedOs/lpptiger) project.

+ Lipid oxidation pathways in `SBML` format
+ List of phospholipids used for *in silico* oxidation by [LPPtiger](https://bitbucket.org/SysMedOs/lpptiger) in `.xlsx` format
+ *in silico* predicted lipid peroxidation products (LPPs) libraries in `sdf` format
+ *in silico* fragmented tandem mass spectra libraries of *in silico* predicted LPPs in `msp` format
+ Summary tables of *in silico* predicted LPPs in `.xlsx` format
+ Summary tables of unique sn1 and sn2 residues of *in silico* predicted LPPs in `.xlsx` format

The predicted structures and corresponding spectra were generated using [LPPtiger](https://bitbucket.org/SysMedOs/lpptiger) using level 1 oxidation with maximum 3 modification sites with the prostanes prediction function enabled.

### Instructions ###
Please navigate to the [Source](https://bitbucket.org/SysMedOs/filesrepository/src) page of this repository or go to [https://bitbucket.org/SysMedOs/filesrepository/src](https://bitbucket.org/SysMedOs/filesrepository/src) to access the files:

The lipid oxidation pathways are stored under [SBML folder](
https://bitbucket.org/SysMedOs/filesrepository/src/c06d51a3cb5a03db77f52452e802208e7a46a9e0/SBML/?at=master).

Corresponding images in `jpg` format are stored under [SBML/SBML_JPG folder](https://bitbucket.org/SysMedOs/filesrepository/src/c06d51a3cb5a03db77f52452e802208e7a46a9e0/SBML/SBML_JPG/?at=master).


Lists of phospholipids used for *in silico* oxidation by [LPPtiger](https://bitbucket.org/SysMedOs/lpptiger) are stored directly under [in-silico-oxidation folder](https://bitbucket.org/SysMedOs/filesrepository/src/c06d51a3cb5a03db77f52452e802208e7a46a9e0/in-silico-oxidation/?at=master).

The *in silico* predicted files are stored in sub-folders under `in-silico-oxidation` folder organized by phospholipids classes.



** Note: **
You can navigate to the [Downloads](https://bitbucket.org/SysMedOs/filesrepository/downloads/) page and click `Download repository` to download all files as a zip package.
[https://bitbucket.org/SysMedOs/filesrepository/downloads/](https://bitbucket.org/SysMedOs/filesrepository/downloads/)



** Recommended software **

We recommend to use following software to process the files in this repository.

+ ** SBML **
    - Cell Designer [http://www.celldesigner.org/](http://www.celldesigner.org/)
+ ** SDF **
    - ChemAxon Instant JChem (Cross platforms) [https://www.chemaxon.com/products/instant-jchem-suite/instant-jchem/](https://www.chemaxon.com/products/instant-jchem-suite/instant-jchem/)
    - Progenesis SDF Studio (Windows platforms only) [http://www.nonlinear.com/progenesis/sdf-studio/](http://www.nonlinear.com/progenesis/sdf-studio/)
+ ** MSP **
    - NIST MS Search Program [http://chemdata.nist.gov/mass-spc/ms-search/](http://chemdata.nist.gov/mass-spc/ms-search/)




### License ###

+ LPPtiger is Dual-licensed
    * For academic and non-commercial use: `Creative Commons CC BY-NC-SA 4.0 License`: 
    
        [The Creative Commons CC BY-NC-SA 4.0 License](https://creativecommons.org/licenses/by-nc-sa/4.0/)

    * For commercial use: please contact the develop team by email.

+ Please cite our publication in an appropriate form. 

     - [Ni, Zhixu, Georgia Angelidou, Ralf Hoffmann, and Maria Fedorova. "LPPtiger software for lipidome-specific prediction and identification of oxidized phospholipids from LC-MS datasets." Scientific Reports 7, Article number: 15138 (2017).](https://www.nature.com/articles/s41598-017-15363-z)
    
        * DOI: [`10.1038/s41598-017-15363-z`](https://www.nature.com/articles/s41598-017-15363-z)


### Further questions? ###

* Read our [wiki](https://bitbucket.org/SysMedOs/lpptiger/wiki/Home)
* Report any issues here: [https://bitbucket.org/SysMedOs/lpptiger/issues](https://bitbucket.org/SysMedOs/lpptiger/issues)


### Fundings ###
We acknowledge all projects that supports the development of LPPtiger:

+ BMBF - Federal Ministry of Education and Research Germany:

    https://www.bmbf.de/en/

+ e:Med Systems Medicine Network:

    http://www.sys-med.de/en/

+ SysMedOS Project : 

    https://home.uni-leipzig.de/fedorova/sysmedos/